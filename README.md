# NexIS：自我監督以及可信賴學習技術的次世代智能服務

## Github Repository 連結

#### 模型

1. [NetFense: 用戶貼標分類同時防止隱私屬性被準確反推](https://github.com/ICHproject/NetFense/)
1. [Direct Floor Plan Estimation: 單張 perspective 或是全景照片 3D Layout 預測](https://github.com/EnriqueSolarte/direct_360_FPE/)
1. [Region based and Diversity Aware Active Learning: 基於區域性與對多樣性敏感的點雲語意分割](https://github.com/tsunghan-wu/ReDAL)
1. [CLIPCAM: 基於文字提示之物體與動作偵測](https://github.com/aiiu-lab/CLIPCAM)
1. [Dual Awareness Attention for Few Shot Object Detection: 工業場域少量樣本資料學習](https://github.com/Tung-I/Dual-awareness-Attention-for-Few-shot-Object-Detection)
1. [D2ADA: 車輛感知跨領域主動資料標記技術](https://github.com/tsunghan-wu/D2ADA)
1. [MonoDTR: 自駕技術單眼鏡頭感知技術 (CVPR’22)](https://github.com/KuanchihHuang/MonoDTR)
1. [MIL Derived Transformer for Weakly Supervised Point Cloud Segmentation: 弱監督點雲分割技術](https://github.com/jimmy15923/wspss_mil_transformer)
1. [FuSta: Hybrid Neural Fusion for Full-frame Video Stabilization](https://github.com/alex04072000/FuSta.git)
1. [DirectVoxGO: Direct Voxel Grid Optimization (CVPR’22)](https://github.com/sunset1995/DirectVoxGO.git)

### 資料集

1. [Casual Conversations Dataset (Additional Annotations)](https://github.com/ChangSongSong/Casual-MetaData.git)
1. [SUPERB](http://superbbenchmark.org/)
1. [OCID Ref](https://github.com/lluma/OCID-Ref.git)
1. [TrUMAn](https://www.cmlab.csie.ntu.edu.tw/project/trope/)
1. [Self-supervised Indoor Layout Prediction Dataset](https://github.com/EnriqueSolarte/direct_360_FPE)
1. [SUPERB hidden set](http://superbbenchmark.org/)
1. [SUPERB+](http://superbbenchmark.org/)
1. [SUPERB++](http://superbbenchmark.org/)
1. [MPNet](https://github.com/cylin-cmlab/MPNet.git)
1. [MNSQA](https://github.com/DanielLin94144/DUAL-textless-SQA)
